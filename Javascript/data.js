var roomArray = [
	{
		//INDEX 0
		title:'Storage Cupboard',
		text:'You’ve just planted the Phase Bomb in the energy core of the ship, and you find yourself hiding in the storage cupboard. It’s time to escape. All that’s left to do is to collect a few pieces to fix a broken escape pod and leave the ship. From what you recall, the enemies on the ship are passive, and can be slow to react.',
		image:'ArtAndMedia/dotJPEG/storageCupboardArt.jpg',
		choices:[
			{
				text:'Look around',
				index:1,
			}
		]
	},
	{
		//INDEX 1
		title:'Storage Cupboard',
		text:"You look around and see dust everywhere, must be a pretty unused cupboard. Shelves line the walls and there's a couple of tool bags on the floor.",
		image:'ArtAndMedia/dotJPEG/storageCupboardArt.jpg',
		choices:[
			{
				text:'Search the shelves.',
				index:2,
			},
			{
				text:'Search the tool bag.',
				index:3,
			}
		]
	},
	{
		//INDEX 2
		title:'Storage Cupboard',
		text:'You search the shelves for anything useful. Unfortunately they are all too old and unusable. ',
		image:'ArtAndMedia/dotJPEG/storageCupboardShelvesArt.jpg',
		choices:[
			{
				text:'Back to storage cupboard',
				index:1,
			}
		]
	},
	{
		//INDEX 3
		title:'Storage Cupboard',
		text:'You search the nearby tool bag and find a wrench that will come in handy when fixing the ship. You put it in your backpack. You feel like it’s time to leave the cupboard and begin the escape.',
		image:'ArtAndMedia/dotJPEG/storageCupboardToolbagArt.jpg',
		item:"wrench",
		choices:[
			{
				text:'Look through the window in the door',
				index:4,

			}
		]
	},
	{
		//INDEX 4
		title:'Storage Cupboard',
		text:'To the left, the ship’s Core where you planted the Phase Bomb. To the right, a hallway with more rooms beyond.',
		image:'ArtAndMedia/dotJPEG/storageCupboardArt.jpg',
		choices:[
			{
				text:'Go left, towards the ship’s Core.',
				index:5,
			},
			{
				text:'Sneak right, towards the hallway.',
				index:6,
			}
		]
	},
	{
		//INDEX 5
		title:'Energy Core',
		text:'You head left, and peek through the door - the Phase Bomb is still attached, you have about 40 minutes to get off this ship. You also notice a piece of paper on the floor by the door: It looks like a code of some sort.',
		item:"code",
		image:'ArtAndMedia/dotJPEG/MainHallwayArt.jpg',
		choices:[
			{
				text:'Back to cupboard',
				index:4,
			},
		]
	},
	{
		//INDEX 6
		title:'Main Hallway',
		text:'You slowly creep right, making sure to avoid making noise as to not draw attention to your presence. You are given more choices - to the right appears to be a toilet; straight ahead looks like the living quarters for the crew; to the left you can hear a large engine whirring away and strange inaudible voices.',
		image:'ArtAndMedia/dotJPEG/MainHallwayArt.jpg',
		choices:[
			{
				text:'Head toward the Toilet',
				index:7,
			},
			{
				text:'Enter the sleeping quarters.',
				index:8,
			},
			{
				text:'Look left into the engine room. REQUIRED: Key',
				index:10,
			}
		]
	},

	{
		//INDEX 7
		title:'Toilet',
		text:"You enter what looks to be the toilet. You notice something strange about the final cubicle but cant put your finger on what it is. You reach the final cubicle and find 'Space Sandals of Silence™'. You consider all of the implications before thinking you've got nothing to lose and putting them on.",
		image:'ArtAndMedia/dotJPEG/ToiletArt.jpg',
		item:"sandals",
		choices:[
			{
				text:'Back to hallway',
				index:6,
			}
		]
	},
	{
		//INDEX 8
		title:'Sleeping Quarters',
		text:'You slowly open the sleeping quarters, and see multiple workers sleeping on bunk beds lining the floor and walls.. It appears to be night time aboard the ship.',
		image:'ArtAndMedia/dotJPEG/SleepingQuartersArt.jpg',
		choices:[
			{
				text:'Back to hallway',
				index:6,
			},
			{
				text:'Creep deeper into the room',
				index:9,
			}
		]
	},
	{
		//INDEX 9
		title:'Sleeping Quarters',
		text:"As you walk deeper and deeper into the room, some of the workers stirr. Luckily you make it to the end of the room without being noticed. You  find a key in the pocket of a worker's jacket.",
		image:'ArtAndMedia/dotJPEG/SleepingQuartersDeeperArt.jpg',
		requirement: true,
		item:"key",
		choices:[
			{
				text:'Back to hallway',
				index:6,
			}
		]
	},
	{
		//INDEX 10
		title:'Engine Room',
		text:'You walk left into the engine room and quickly hide behind some large machinery to avoid being seen. Some workers appear to still be up maintaining the engine. As you look around you, see some components that could come in handy. You can sit tight to see if the workers leave, or make a run for the components and out through the door just past them.',
		image:'ArtAndMedia/dotJPEG/EngineRoomEntryArt.jpg',
		requirement: true,
		choices:[
			{
				text:'Risk grabbing the components',
				index:11,
			},
			{
				text:'Wait a little longer',
				index:26,
			}
		]
	},
	{
		//INDEX 11
		title:'Engine Room',
		text:'You rush to your feet and hurry toward the components on the side. The enemies slowly turn towards you but by the time they can do anything about it you dissapear through the opposite door, shutting it tightly behind you. ',
		image:'ArtAndMedia/dotJPEG/EngineRoomGrabArt.jpg',
		item:"hardware",
		choices:[
			{
				text:'Continue and look around.',
				index:12,
			},
		]
	},
	{
		//INDEX 12
		title:'Abandoned Corridor',
		text:'You continue forward, through a corridor that looks all but abandoned. At the end lies a door. You hear a faint alarm coming from the engine room and assume the enemies will be close behind.',
		image:'ArtAndMedia/dotJPEG/VentHallwayEntryArt.jpg',
		choices:[
			{
				text:'Open and go through the vent.',
				index:13,
			},
			{
				text:'Wait in a nearby locker.',
				index:14,
			}
		]
	},
	{
		//INDEX 13
		title:'Vent',
		text:'You quickly dive into the open vent at the end of the corridor. Footsteps pound the ground in the corridor behind you. ',
		image:'ArtAndMedia/dotJPEG/VentHallwayEscapeArt.jpg',
		choices:[
			{
				text:'Continue and look around.',
				index:15,
			},
		]
	},
	{
		//INDEX 14
		title:'Abandoned Corridor',
		text:'You clumsily rush into a nearby locker and stay as silent as you can. However the species on this ship seem to have caught on to your idea. They seek you out, search you, then kill you.',
		image:'ArtAndMedia/dotJPEG/VentHallwayLockerDeathArt.jpg',
		choices:[
			{
			text:'Open and go through the vent.',
			index:13,
			}
		]
	},
		{
		//INDEX 15
		title:'Vent',
		text:'After quickly surveying the vents and where they lead, you see various options. It looks like the vent leads right to the Sleeping quarters, and left into some sort of kitchen. The vent looks like it continues past the kitchen but there’s a grate blocking you from entering via vent.',
		image:'ArtAndMedia/dotJPEG/VentLookAround.jpg',
		choices:[
			{
				text:'Head towards the kitchen.',
				index:17,
			},
			{
				text:'Head toward the sleeping quarters.',
				index:16,
			}
		]
	},
	{
		//INDEX 16
		title:'Sleeping Quaters',
		text:"You've already been here...might want to consider leaving before they wake up!",
		image:'ArtAndMedia/dotJPEG/SleepingQuartersDeeperArt.jpg',
		choices:[
			{
				text:'Back to vent',
				index:15,
			}
		]
	},
	{
		//INDEX 17
		title:'Kitchen',
		text:'You slowly crawl towards the entrance of the kitchen and it looks clear. You drop down into the room. There might be something useful here, or you can continue to the escape pod and leave the ship to its soon demise.',
		image:'ArtAndMedia/dotJPEG/KitchenEntranceArt.jpg',
		choices:[
			{
				text:'Look around',
				index:18,
			},
			{
				text:'Head to the escape pod',
				index:19,
			}
		]
	},
	{
		//INDEX 18
		title:'Kitchen',
		text:'You quickly scan the place, opening cupboards and drawers. In the corner there’s some kind of glowing pad...this could go one of two ways...',
		image:'ArtAndMedia/dotJPEG/KitchenSearchArt.jpg',
		choices:[
			{
				text:'Step on the pad',
				index:21,
			},
			{
				text:'Ignore and head to the escape pod',
				index:19,
			}
		]
	},
	{
		//INDEX 19
		title:'Escape Pod Bay',
		text:'You climb up into the other vent and arrive above the broken escape pod. There appears to be nobody around, so you jump down. Did you have all the pieces?',
		image:'EscapePodRoom.jpg',
		choices:[
		{
			text:'Continue...',
			index:20,
		}
	]
},
{
//INDEX 20
title:'This will be a deathscreen',
text:'You slowly crawl towards the entrance of the kitchen and it looks clear. You drop down into the room. ',
image:'ArtAndMedia/dotJPEG/EscapePodRoom.jpg',
requirement:true,
choices:[
{
	text:'Back to vent',
	index:15,
}
]
},
{
//INDEX 21
title:'Kitchen / Void',
text:'You hesitantly step onto the pad. It turns from a white glow to a green glow and pulses at an increasing speed. You see your legs dissipate below you as it consumes your whole body. You are suddenly plunged into limbo-type darkness with a loud screech of interference...Two choices appear in your head.',
image:'ArtAndMedia/dotJPEG/LimbiQuestionsArt.jpg',
choices:[
{
	text:'Submit and become a Voon',
	index:23,
},
{
	text:'Death.',
	index:22,
}
]
},
{
//INDEX 22
title:'Void',
text:"It looks like you've just been left...in this limbo-esque space. Oops.",
image:'ArtAndMedia/dotJPEG/LimboDeathArt.jpg',
choices:[
{
	text:'Continue',
	index:23,
}
]
},
{
//INDEX 23
title:'Void',
text:"You suddenly appear on some sort of operating table with multiple Voons surrounding you. They are all holding syringes with some sort of deep blue liquid. You feel about 14 sharp pains all the way down your body before passing out again. ",
image:'ArtAndMedia/dotJPEG/OperatingTableArt.jpg',
choices:[
{
	text:'Wake up...',
	index:24,
}
]
},
{
//INDEX 24
title:'Your Room',
text:"You wake up the same as you would any day, ready to begin work on maintaining improving the Voonian IT and Network system. As you get dressed and prepare for the day you make yourself some Zwi-Tea. Just what you needed to kickstart the day...",
image:'ArtAndMedia/dotJPEG/VoonianRoomArt.jpg',
choices:[
{
	text:'Head to work...',
	index:25,
}
]
},
{
//INDEX 25
title:'Hallway',
text:"You lock up and head down the hallway for work. Fellow Voons around you seem at unease and you hear alarms going off at the other end of the ship… Escape pods are being used....You barely had enough time to finish up your Zwi-Tea before the ship is blown to smithereens. ",
image:'ArtAndMedia/dotJPEG/VoonHallwayWork.jpg',
choices:[
{
	text:'Finish.',
	index:24,
}
]
},
{
//INDEX 26
title:'Engine Room',
text:"You sit tight, in the hope that they head off to bed like the rest. However one of them stumbles over towards you and wonders what you are.",
image:'ArtAndMedia/dotJPEG/EngineRoomEntryArt.jpg',
choices:[
{
	text:'Continue',
	index:26,
}
]
},
];

//var objectArray [{}]

//create code var.
localStorage.setItem("CODEVAR", false);
//create sandals var.
localStorage.setItem("SANDALSVAR", false);
//create wrench var.
localStorage.setItem("WRENCHVAR", false);
//create hardware var.
localStorage.setItem("HARDWAREVAR", false);
//create canister var.
localStorage.setItem("CANISTERVAR", false);
//create key var.
localStorage.setItem("KEYVAR", false);

function OnLoad()
{
	SelectRoom(0);
	DisplayPlayerInfo();
}

function SelectRoom(roomIndex)
{
	console.log("room entered "+roomIndex);




//check to see if a room requires an item to enter.
	if(roomArray[roomIndex].requirement != null)
	{
		var sandalCheck = localStorage.getItem("SANDALSVAR");
		var keyCheck = localStorage.getItem("KEYVAR");
		var wrenchCheck = localStorage.getItem("WRENCHVAR");
		var hardwareCheck = localStorage.getItem("HARDWAREVAR");
		var codeCheck = localStorage.getItem("CODEVAR");
		//console.log("SANDALSVAR:	" + sandalCheck);
		//console.log("room index: " + roomIndex);

		if (roomIndex == 9 && sandalCheck == 'false')
		{
			document.location.href='sandalDeath.html';
			console.log("YOU DONT HAVE SANDALS");
		}

	 if (roomIndex == 10 && keyCheck == 'false') {
			//document.location.href='gameSetup.html';
			alert("You need a key to enter")
			roomIndex = 6;
			console.log("YOU DO NOT OWN A KEY");
		}


		if (roomIndex == 20 && codeCheck == 'false') {
			document.location.href='noCodeDeath.html';
		} else if (roomIndex == 20 && wrenchCheck == 'false') {
			document.location.href='noWrenchDeath.html';
		} else if (roomIndex == 20 && hardwareCheck == 'false') {
			document.location.href='noHardwareDeath.html';
		} else if (roomIndex == 20) {
			document.location.href='success.html'
		}
	}



	//check room for item property.
	if (roomArray[roomIndex].item != null)
	{
		//check if its sandals
		if (roomArray[roomIndex].item == "sandals")
		{
			//set local storage var to true & display it in inventory.
			localStorage.setItem("SANDALSVAR", true);
			document.getElementById("sandals").style.display = 'block';
			console.log("Space sandals of silence found.");
		}
		//check if its wrench
		if (roomArray[roomIndex].item == "wrench")
		{
			//set local storage var to true & display it in inventory.
			localStorage.setItem("WRENCHVAR", true);
			document.getElementById("wrench").style.display = 'block';
			console.log("Wrench found.");
		}
		//check if its hardware
		if (roomArray[roomIndex].item == "hardware")
		{
			//set local storage var to true & display it in inventory.
			localStorage.setItem("HARDWAREVAR", true);
			document.getElementById("hardware").style.display = 'block';
			console.log("hardware found.");
		}
			//set local storage vart to true & display it in inventory.
		if (roomArray[roomIndex].item == "key") {
			localStorage.setItem("KEYVAR", true);
			document.getElementById("keys").style.display = 'block';
			console.log("Engine room key found");
		}
		if (roomArray[roomIndex].item == "code") {
			localStorage.setItem("CODEVAR", true);
			document.getElementById("code").style.display = 'block';
			console.log("strange code found");
		}
	}

	if (roomIndex == 22) {
		document.location.href='lockerDeath.html';
		console.log("rip");
	}
	if (roomIndex == 14) {
		document.location.href='lockerDeath.html';
		console.log("YOU MADE AN INCORRECT DECISION");
	}
	if (roomIndex == 26) {
		document.location.href='waitingEngineDeath.html';
		console.log("Incorrect decision");
	}

	document.getElementById("roomTitle").innerHTML = roomArray[roomIndex].title;
	document.getElementById("roomText").innerHTML = roomArray[roomIndex].text;

	//localStorage.setItem("roomArt", roomArray[roomIndex].roomimage);

	var imagesrc = "<img src='" + roomArray[roomIndex].image +  "'>"

	document.getElementById("roomArt").innerHTML = imagesrc;

	document.getElementById("roomChoices").innerHTML = "";

	for(var i = 0; i < roomArray[roomIndex].choices.length; i++)
	{
		var choiceButton = "<button class='btn-primary btn-block btn-lg	' onclick='SelectRoom(" + roomArray[roomIndex].choices[i].index + ")'>" + roomArray[roomIndex].choices[i].text + "</button>";

		document.getElementById("roomChoices").innerHTML += choiceButton;
	}
}

function StorePlayerData() {
  //var playerName = document.forms[0]["playername"].value;
  localStorage.setItem("playerName", document.forms[0]["playername"].value);
  //var playerColour = document.forms[0]["playercolour"].value;
  localStorage.setItem("playerColour", document.forms[0]["playercolour"].value);

	localStorage.setItem("playerAge", document.forms[0]["playerage"].value);

	localStorage.setItem("roomArt", roomArray[roomIndex].roomimage);
}

function DisplayPlayerInfo() {

	var name= localStorage.getItem("playerName");
	var age= localStorage.getItem("playerAge");
	var colour= localStorage.getItem("playerColour");

	document.getElementById("playername").innerHTML += name;
	document.getElementById("playerage").innerHTML += age;
	//document.getElementById("playercolour").innerHTML += colour;

	SetBackgroundColour();
//	document.getElementById("").style.background = colour;
}
function SetBackgroundColour() {
	var colour = localStorage.getItem("playerColour");

  var x = document.getElementsByClassName("well");

  for (var i = 0; i < x.length; i++) {
	    x[i].style.backgroundColor = colour;
  }
}
function validateForm() {
    var x = document.forms[0]["playerage"].value;
    if (x < 0 || x > 120) {
				document.forms[0]["playerage"].value = "";
        alert("Please enter a valid age.");
    }
}
$('#mapModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
//*function updateText() {
//	var x = document.forms[0]["playercolour"].value;
//	if
//}
